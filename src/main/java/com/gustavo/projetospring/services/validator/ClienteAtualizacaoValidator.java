package com.gustavo.projetospring.services.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.dto.ClienteDTO;
import com.gustavo.projetospring.repositories.ClienteRepository;
import com.gustavo.projetospring.resources.exceptions.CampoValor;

public class ClienteAtualizacaoValidator implements ConstraintValidator<ClienteAtualizacao, ClienteDTO> {
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	HttpServletRequest httpRequest;
	
	@Override
	public void initialize(ClienteAtualizacao ann) {
	}

	@Override
	public boolean isValid(ClienteDTO objDto, ConstraintValidatorContext context) {
		List<CampoValor> list = new ArrayList<>();
		
		//obter o idcliente da URI
		Map<String, String> map = (Map<String, String>) httpRequest.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		Integer idCliente = Integer.parseInt(map.get("id"));
				

		// verifica se o email pertence ao cliente q esta atualizando
		Cliente cli = clienteRepository.findByEmail(objDto.getEmail());
		if (cli != null && !cli.getId().equals(idCliente)) {
			list.add(new CampoValor("email", "email ja cadastrado para o cliente " + cli.getNome()));
		}
		
		//cria a lista de erros do framework q sera retornada para o metodo de validacao
		for (CampoValor e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getValor()).addPropertyNode(e.getCampo())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}