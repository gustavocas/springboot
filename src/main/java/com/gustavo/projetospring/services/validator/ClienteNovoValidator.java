package com.gustavo.projetospring.services.validator;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.domain.enums.TipoCliente;
import com.gustavo.projetospring.dto.ClienteNovoDTO;
import com.gustavo.projetospring.repositories.ClienteRepository;
import com.gustavo.projetospring.resources.exceptions.CampoValor;

public class ClienteNovoValidator implements ConstraintValidator<ClienteNovo, ClienteNovoDTO> {
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public void initialize(ClienteNovo ann) {
	}

	@Override
	public boolean isValid(ClienteNovoDTO objDto, ConstraintValidatorContext context) {
		List<CampoValor> list = new ArrayList<>();

		// inclua os testes aqui, inserindo erros na lista
		if (objDto.getTipo() == null) {
			list.add(new CampoValor("tipo", "Tipo nao pode ser nulo"));
		}else if (objDto.getTipo().equals(TipoCliente.PESSOA_FISICA.getCodigo())) {
			if (objDto.getDocumento().length()!=11) {
				list.add(new CampoValor("documento", "CPF Invalido"));
			}			
		}else if (objDto.getTipo().equals(TipoCliente.PESSOA_JIRIDICA.getCodigo())) {
			if (objDto.getDocumento().length()!=11) {
				list.add(new CampoValor("documento", "CNPJ Invalido"));
			}
		}else {
			list.add(new CampoValor("Tipo", "Tipo tipo de cliente invalido"));
		}
		
		Cliente cli = clienteRepository.findByEmail(objDto.getEmail());
		if (cli != null) {
			list.add(new CampoValor("email", "email ja cadastrado para o cliente " + cli.getNome()));
		}
		
		//cria a lista de erros do framework q sera retornada para o metodo de validacao
		for (CampoValor e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getValor()).addPropertyNode(e.getCampo())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}