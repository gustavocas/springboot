package com.gustavo.projetospring.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Estado;
import com.gustavo.projetospring.repositories.EstadoRepository;

@Service
public class EstadoService {
	 
	@Autowired
	private EstadoRepository estadoRepository;
	
	public Estado buscar(Integer id) {
		Optional<Estado> obj = estadoRepository.findById(id);
		return obj.orElseThrow(() -> new com.gustavo.projetospring.services.exceptions.ObjectNotFoundException("Estado nao encontrado com o codigo: " + id));
	}
	
	public List<Estado> listar() {
		List<Estado> obj = estadoRepository.findAllByOrderByNome();
		return obj;
	}
	
	public Estado salvar(Estado uf) {
		estadoRepository.saveAll(Arrays.asList(uf));
		return uf;
	}

}
