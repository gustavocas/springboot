package com.gustavo.projetospring.services.exceptions;

public class ChaveVioladaException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ChaveVioladaException(String message) {
		super(message);
	}
	
	public ChaveVioladaException(String message, Throwable cause) {
		super(message, cause);
	}

}
