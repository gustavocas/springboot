package com.gustavo.projetospring.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Categoria;
import com.gustavo.projetospring.domain.Produto;
import com.gustavo.projetospring.repositories.CategoriaRepository;
import com.gustavo.projetospring.repositories.ProdutoRepository;

@Service
public class ProdutoService {
	 
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Produto buscar(Integer id) {
		Optional<Produto> obj = produtoRepository.findById(id);
		return obj.orElseThrow(() -> new com.gustavo.projetospring.services.exceptions.ObjectNotFoundException("Produto nao encontrado com o codigo: " + id));
	}
	
	public Page<Produto> listarPaginado(String nome, List<Integer> idCategoria, Integer pagina, Integer linhasPorPagina, String ordenacao, String direcao){
		PageRequest pageReq = PageRequest.of(pagina, linhasPorPagina, Direction.valueOf(direcao), ordenacao);
		List <Categoria> categorias = categoriaRepository.findAllById(idCategoria);
		return produtoRepository.findDistinctByNomeContainsAndCategoriasIn(nome, categorias, pageReq);
	}

}
