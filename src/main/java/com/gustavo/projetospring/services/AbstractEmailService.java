package com.gustavo.projetospring.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Pedido;

@Service
public abstract class AbstractEmailService implements EmailService {

	@Value("${default.email}") //definido no application.properties
	private String remetente; 
	
	@Override
	public void enviarEmailConfirmacao(Pedido obj) {
		SimpleMailMessage sm = formatarEmail(obj);
		sendEmail(sm);
	}

	protected SimpleMailMessage formatarEmail(Pedido obj) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(obj.getCliente().getEmail());
		sm.setFrom(remetente);
		sm.setSubject("Confirmação de pedido: "+obj.getId());
		sm.setSentDate(new Date());
		sm.setText(obj.toString());
		return sm;
	};
	
}
