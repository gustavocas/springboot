package com.gustavo.projetospring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Cidade;
import com.gustavo.projetospring.domain.Estado;
import com.gustavo.projetospring.repositories.CidadeRepository;

@Service
public class CidadeService {
	 
	@Autowired
	private CidadeRepository cidadeRepository;
	
	
	public List<Cidade> listarPorEstado(Integer idEstado) {
		Estado estado = new Estado(idEstado,"");
		List<Cidade> obj = cidadeRepository.findByEstado(estado);
		return obj;
	}
	

}
