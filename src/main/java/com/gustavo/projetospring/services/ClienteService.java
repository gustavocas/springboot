package com.gustavo.projetospring.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.security.auth.login.LoginException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gustavo.projetospring.domain.Cidade;
import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.domain.Endereco;
import com.gustavo.projetospring.domain.enums.PerfilCliente;
import com.gustavo.projetospring.domain.enums.TipoCliente;
import com.gustavo.projetospring.dto.ClienteDTO;
import com.gustavo.projetospring.dto.ClienteNovoDTO;
import com.gustavo.projetospring.repositories.ClienteRepository;
import com.gustavo.projetospring.repositories.EnderecoRepository;
import com.gustavo.projetospring.security.UserSS;
import com.gustavo.projetospring.services.exceptions.ChaveVioladaException;
import com.gustavo.projetospring.services.exceptions.ObjectNotFoundException;

@Service
public class ClienteService {
	 
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public Cliente buscar(Integer id) throws LoginException {
		Optional<Cliente> obj = clienteRepository.findById(id);
		
		UserSS usuario = UserService.usuarioAutenticado();
		if (usuario == null || !usuario.hasRole(PerfilCliente.ADMIN) && !id.equals(usuario.getId())) {
			//throw new LoginException("Acesso negado!!!!! :*");
		}
		
		return obj.orElseThrow(() -> new com.gustavo.projetospring.services.exceptions.ObjectNotFoundException("Cliente nao encontrado com o codigo: " + id));
	}
	
	public Cliente buscarPorEmail(String email) throws LoginException {
		Cliente obj = clienteRepository.findByEmail(email);
		
//		UserSS usuario = UserService.usuarioAutenticado();
//		if (usuario == null || !usuario.hasRole(PerfilCliente.ADMIN) && !usuario.getUsername().equals(email)) {
//			throw new LoginException("Acesso negado!!!!! :*");
//		}
		
		if (obj==null) {
			throw new ObjectNotFoundException("Cliente nao encontrado com o email: " + email);
		}
		
		return obj;
	}
	
	@Transactional
	public Cliente salvar(Cliente cli) {
		cli.setId(null);
		clienteRepository.saveAll(Arrays.asList(cli));
		enderecoRepository.saveAll(cli.getEnderecos());
		return cli;
	}
	
	public Cliente atualizar(Cliente cat) throws LoginException {
		buscar(cat.getId()); //verifica se o id existe. se nao existir o metodo buscar lanca excecao
		clienteRepository.saveAll(Arrays.asList(cat));
		return cat;
	}

	public void remover(Integer id) throws LoginException {
		buscar(id); //verifica se o id existe. se nao existir o metodo buscar lanca excecao
		try {
			clienteRepository.deleteById(id);
		}catch (DataIntegrityViolationException e) {
			throw new ChaveVioladaException("Cliente possui items relacionados.");
		}
	}

	public List<Cliente> listarTodas() {
		return clienteRepository.findAll();
	}
	
	public Page<Cliente> listarPaginado(Integer pagina, Integer linhasPorPagina, String ordenacao, String direcao){
		PageRequest pageReq = PageRequest.of(pagina, linhasPorPagina, Direction.valueOf(direcao), ordenacao);
		return clienteRepository.findAll(pageReq);
	}
	
	public Cliente convertDTO(ClienteDTO dto) throws LoginException {
		Cliente cli = buscar(dto.getId());
		cli.setNome(dto.getNome());
		cli.setEmail(dto.getEmail());
		cli.setFoto(dto.getFoto());
		return cli;
	}

	public Cliente convertDTO(@Valid ClienteNovoDTO dto) {
		Cliente cli = new Cliente(null,dto.getNome(), dto.getEmail(), dto.getDocumento(), TipoCliente.toEnum(dto.getTipo()), (dto.getSenha()), dto.getFoto());
		Endereco end = new Endereco(null,dto.getLogradouro(),dto.getNumero(),dto.getComplemento(), dto.getBairro(), dto.getCep(), cli, new Cidade(dto.getIdCidade(),null,null));
		cli.getEnderecos().add(end);
		cli.getTelefones().add(dto.getTelefone1());
		if (dto.getTelefone2()!= null) {
			cli.getTelefones().add(dto.getTelefone2());
		}
		if (dto.getTelefone3()!= null) {
			cli.getTelefones().add(dto.getTelefone3());
		}
		return cli;
	}

}
