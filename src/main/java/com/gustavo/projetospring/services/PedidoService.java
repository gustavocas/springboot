package com.gustavo.projetospring.services;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javax.security.auth.login.LoginException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Categoria;
import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.domain.ItemPedido;
import com.gustavo.projetospring.domain.PagamentoBoleto;
import com.gustavo.projetospring.domain.Pedido;
import com.gustavo.projetospring.domain.Produto;
import com.gustavo.projetospring.domain.enums.EstadoPagamento;
import com.gustavo.projetospring.repositories.ItemPedidoRepository;
import com.gustavo.projetospring.repositories.PagametoRepository;
import com.gustavo.projetospring.repositories.PedidoRepository;
import com.gustavo.projetospring.security.UserSS;

@Service
public class PedidoService {
	 
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private PagametoRepository pagamentoRepository;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private EmailService emailService;
	
	public Pedido buscar(Integer id) {
		Optional<Pedido> obj = pedidoRepository.findById(id);
		return obj.orElseThrow(() -> new com.gustavo.projetospring.services.exceptions.ObjectNotFoundException("Pedido nao encontrado com o codigo: " + id));
	}
	
	
	public Pedido salvar(Pedido ped) throws LoginException {
		ped.setDataHora(new Date());
		ped.getPagamento().setEstado(EstadoPagamento.PENDENTE);
		ped.getPagamento().setPedido(ped);
		ped.setCliente(clienteService.buscar(ped.getCliente().getId()));
		
		if (ped.getPagamento() instanceof PagamentoBoleto) {
			PagamentoBoleto pagto = (PagamentoBoleto) ped.getPagamento();
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, 1);
			pagto.setDataVencimento(c.getTime());
		}
		
		pedidoRepository.save(ped);
		pagamentoRepository.save(ped.getPagamento());
		
		for (ItemPedido i : ped.getItens()) {
			i.setDesconto(0.0);
			Produto prod = produtoService.buscar(i.getProduto().getId());
			i.setPreco(prod.getPreco());
			i.setProduto(prod);
			i.setPedido(ped);
		}
		itemPedidoRepository.saveAll(ped.getItens());
		emailService.enviarEmailConfirmacao(ped);
		return ped;
	}
	
	public Page<Pedido> listarPaginado(Integer pagina, Integer linhasPorPagina, String ordenacao, String direcao) throws LoginException{
		PageRequest pageReq = PageRequest.of(pagina, linhasPorPagina, Direction.valueOf(direcao), ordenacao);
		
		UserSS usuario = UserService.usuarioAutenticado();
		if (usuario==null) {
			throw new LoginException("Usuario nao logado :* ");
		}
		
		Cliente cliente = clienteService.buscar(usuario.getId());
		
		return pedidoRepository.findByCliente(cliente, pageReq);
	}

}
