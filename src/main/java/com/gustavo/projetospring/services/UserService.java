package com.gustavo.projetospring.services;

import org.springframework.security.core.context.SecurityContextHolder;

import com.gustavo.projetospring.security.UserSS;

public class UserService {

	public static UserSS usuarioAutenticado() {
		try {
			return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}catch(Exception e) {
			return null;
		}
	}
}
