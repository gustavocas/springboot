package com.gustavo.projetospring.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Categoria;
import com.gustavo.projetospring.dto.CategoriaDTO;
import com.gustavo.projetospring.repositories.CategoriaRepository;
import com.gustavo.projetospring.services.exceptions.ChaveVioladaException;

@Service
public class CategoriaService {
	 
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Categoria buscar(Integer id) {
		Optional<Categoria> obj = categoriaRepository.findById(id);
		return obj.orElseThrow(() -> new com.gustavo.projetospring.services.exceptions.ObjectNotFoundException("Categoria nao encontrada com o codigo: " + id));
	}
	
	public Categoria salvar(Categoria cat) {
		categoriaRepository.saveAll(Arrays.asList(cat));
		return cat;
	}

	public Categoria atualizar(Categoria cat) {
		buscar(cat.getId()); //verifica se o id existe. se nao existir o metodo buscar lanca excecao
		categoriaRepository.saveAll(Arrays.asList(cat));
		return cat;
	}

	public void remover(Integer id) {
		buscar(id); //verifica se o id existe. se nao existir o metodo buscar lanca excecao
		try {
		categoriaRepository.deleteById(id);
		}catch (DataIntegrityViolationException e) {
			throw new ChaveVioladaException("Categoria possui items relacionados.");
		}
	}

	public List<Categoria> listarTodas() {
		return categoriaRepository.findAll();
	}
	
	public Page<Categoria> listarPaginado(Integer pagina, Integer linhasPorPagina, String ordenacao, String direcao){
		PageRequest pageReq = PageRequest.of(pagina, linhasPorPagina, Direction.valueOf(direcao), ordenacao);
		return categoriaRepository.findAll(pageReq);
	}
	
	public Categoria convertDTO(CategoriaDTO dto) {
		return new Categoria(dto.getId(), dto.getNome());
	}

}
