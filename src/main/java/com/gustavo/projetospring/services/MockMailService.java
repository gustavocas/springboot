package com.gustavo.projetospring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public class MockMailService extends AbstractEmailService{
	
	private static final Logger log = LoggerFactory.getLogger(MockMailService.class);

	@Override
	public void sendEmail(SimpleMailMessage smm) {
		log.info("Simulando email:");
		log.info(smm.toString());
		log.info("Email enviado!!");
	}

}
