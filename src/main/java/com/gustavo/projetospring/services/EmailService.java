package com.gustavo.projetospring.services;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.gustavo.projetospring.domain.Pedido;

@Service
public interface EmailService {
	
	void enviarEmailConfirmacao(Pedido obj);
	
	void sendEmail(SimpleMailMessage smm);

}
