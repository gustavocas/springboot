package com.gustavo.projetospring.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.projetospring.domain.Estado;
import com.gustavo.projetospring.services.EstadoService;

@RestController
@RequestMapping(value="/estados")
public class EstadoResource {
	
	@Autowired
	private EstadoService estadoService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {

			Estado obj = estadoService.buscar(id);			
			return ResponseEntity.ok().body(obj);
		
		
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> find() {

		List<Estado> obj = estadoService.listar();			
		return ResponseEntity.ok().body(obj);
		
		
	}
	
	@RequestMapping(value = "/{nome}", method = RequestMethod.POST)
	public ResponseEntity<?> salvar(@PathVariable String nome) {
		
		Estado uf = new Estado(null,nome);
		estadoService.salvar(uf);
		
		return ResponseEntity.ok().body("Salvo com sucesso: " + uf.getId());
	}

}
