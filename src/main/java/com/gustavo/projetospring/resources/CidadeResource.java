package com.gustavo.projetospring.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.projetospring.domain.Cidade;
import com.gustavo.projetospring.domain.Estado;
import com.gustavo.projetospring.services.CidadeService;

@RestController
@RequestMapping(value="/cidades")
public class CidadeResource {
	
	@Autowired
	private CidadeService cidadeService;

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> findPorEstado(@RequestParam(value="idestado", defaultValue = "0")  Integer idEstado) {

		List<Cidade> obj = cidadeService.listarPorEstado(idEstado);			
		return ResponseEntity.ok().body(obj);
		
		
	}
	

}
