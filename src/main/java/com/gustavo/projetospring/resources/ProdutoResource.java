package com.gustavo.projetospring.resources;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.projetospring.domain.Produto;
import com.gustavo.projetospring.dto.ProdutoDTO;
import com.gustavo.projetospring.services.ProdutoService;

@RestController
@RequestMapping(value="/produtos")
public class ProdutoResource {
	
	@Autowired
	private ProdutoService produtoService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {

			Produto obj = produtoService.buscar(id);			
			return ResponseEntity.ok().body(obj);
		
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<ProdutoDTO>> listarPaginado(
			@RequestParam(value="nome", defaultValue = "") String nome, 
			@RequestParam(value="categorias", defaultValue = "0") String categorias, 
			@RequestParam(value="pagina", defaultValue = "0") Integer pagina, 
			@RequestParam(value="linhasPorPagina", defaultValue = "10") Integer linhasPorPagina, 
			@RequestParam(value="ordenacao", defaultValue = "nome") String ordenacao, 
			@RequestParam(value="direcao", defaultValue = "ASC") String direcao) {
		
		//retira os caracteres de endereco URL para texto com espacos e acentos
		String nomeTratado = "";
		try {
			nomeTratado = URLDecoder.decode(nome, "UTF-8");
		}catch (Exception e) {
			nomeTratado = "";
		}
		
		//comando lambda para converter string de categoria para lista de inteiro
		List<Integer> idsCategoria = Arrays.asList(categorias.split(",")).stream().map(x -> Integer.parseInt(x)).collect(Collectors.toList());
		
		Page<Produto> lista = produtoService.listarPaginado(nomeTratado, idsCategoria,pagina,linhasPorPagina,ordenacao, direcao);
		//converte a categoria em DTO para omitir a lista de produto da categori.
		//isso eh feito criando um construtor no DTO que nao possui a lista de produto
		//esse comando abaixo varrea a lista de categorias adicionando na lista de categoriaDTO:
		Page<ProdutoDTO> listaDTO = lista.map(obj -> new ProdutoDTO(obj)); 
		return ResponseEntity.ok().body(listaDTO);
	}

}
