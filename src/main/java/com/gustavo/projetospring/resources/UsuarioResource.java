package com.gustavo.projetospring.resources;

import javax.security.auth.login.LoginException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.dto.CredenciaisDTO;
import com.gustavo.projetospring.services.ClienteService;

@RestController
@RequestMapping(value="/usuario")
public class UsuarioResource {
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@RequestMapping(method = RequestMethod.POST)
	public @Valid CredenciaisDTO salvar(@Valid @RequestBody CredenciaisDTO dto) throws LoginException {
		if ("".equals(dto.getEmail())) {
			throw new LoginException("Usuario invalido");
		}
		Cliente cli = clienteService.buscarPorEmail(dto.getEmail());
		
		if (!(dto.getSenha()).equals(cli.getSenha())) {
			throw new LoginException("Senha invalida");
		}
		
		dto.setToken("chaveQuePrecisoAprenderGerar");
		clienteService.buscarPorEmail(dto.getEmail());
		
		return dto;
		
	}

}
