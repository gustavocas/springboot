package com.gustavo.projetospring.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.security.auth.login.LoginException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.dto.ClienteDTO;
import com.gustavo.projetospring.dto.ClienteNovoDTO;
import com.gustavo.projetospring.services.ClienteService;

@RestController
@RequestMapping(value="/clientes")
public class ClienteResource {
	
	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) throws LoginException {

			Cliente obj = clienteService.buscar(id);			
			return ResponseEntity.ok().body(obj);
		
		
	}
	
	@RequestMapping(value = "/email", method = RequestMethod.GET)
	public ResponseEntity<Cliente> find(@RequestParam(value = "value") String email) throws LoginException {

			Cliente obj = clienteService.buscarPorEmail(email);			
			return ResponseEntity.ok().body(obj);
		
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ClienteDTO>> listarTodas() {
		List<Cliente> lista = clienteService.listarTodas();
		//converte a categoria em DTO para omitir a lista de produto da categori.
		//isso eh feito criando um construtor no DTO que nao possui a lista de produto
		//esse comando abaixo varrea a lista de categorias adicionando na lista de categoriaDTO:
		List<ClienteDTO> listaDTO = lista.stream().map(obj -> new ClienteDTO(obj)).collect(Collectors.toList()); 
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@RequestMapping(value = "/pagina",method = RequestMethod.GET)
	public ResponseEntity<Page<ClienteDTO>> listarPaginado(
			@RequestParam(value="pagina", defaultValue = "0") Integer pagina, 
			@RequestParam(value="linhasPorPagina", defaultValue = "10") Integer linhasPorPagina, 
			@RequestParam(value="ordenacao", defaultValue = "nome") String ordenacao, 
			@RequestParam(value="direcao", defaultValue = "ASC") String direcao) {
		Page<Cliente> lista = clienteService.listarPaginado(pagina,linhasPorPagina,ordenacao, direcao);
		//converte a categoria em DTO para omitir a lista de produto da categori.
		//isso eh feito criando um construtor no DTO que nao possui a lista de produto
		//esse comando abaixo varrea a lista de categorias adicionando na lista de categoriaDTO:
		Page<ClienteDTO> listaDTO = lista.map(obj -> new ClienteDTO(obj)); 
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@Valid @RequestBody ClienteNovoDTO dto) {
		Cliente cat = clienteService.convertDTO(dto);
		cat = clienteService.salvar(cat);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cat.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/foto", method = RequestMethod.POST) 
	public ResponseEntity<Void> salvarFoto(@RequestBody ClienteDTO dto) throws LoginException {
		Cliente cat;
		cat = clienteService.convertDTO(dto);
		cat = clienteService.atualizar(cat);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cat.getId()).toUri();
		return ResponseEntity.created(uri).build();
		
	}
	
	@RequestMapping(value = "/{id}",method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@Valid @PathVariable Integer id, @Valid @RequestBody ClienteDTO catDTO) throws LoginException {
		catDTO.setId(id);
		Cliente cat = clienteService.convertDTO(catDTO);
		cat.setId(id);
		cat = clienteService.atualizar(cat);
		
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<Void> remover(@PathVariable Integer id) throws LoginException {
		clienteService.remover(id);
		
		return ResponseEntity.noContent().build();
	}

}
