package com.gustavo.projetospring.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gustavo.projetospring.domain.Categoria;
import com.gustavo.projetospring.dto.CategoriaDTO;
import com.gustavo.projetospring.services.CategoriaService;

@RestController
@RequestMapping(value="/categorias")
public class CategoriaResource {
	
	@Autowired
	private CategoriaService categoriaService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Categoria> find(@PathVariable Integer id) {
		Categoria obj = categoriaService.buscar(id);			
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CategoriaDTO>> listarTodas() {
		List<Categoria> lista = categoriaService.listarTodas();
		//converte a categoria em DTO para omitir a lista de produto da categori.
		//isso eh feito criando um construtor no DTO que nao possui a lista de produto
		//esse comando abaixo varrea a lista de categorias adicionando na lista de categoriaDTO:
		List<CategoriaDTO> listaDTO = lista.stream().map(obj -> new CategoriaDTO(obj)).collect(Collectors.toList()); 
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@RequestMapping(value = "/pagina",method = RequestMethod.GET)
	public ResponseEntity<Page<CategoriaDTO>> listarPaginado(
			@RequestParam(value="pagina", defaultValue = "0") Integer pagina, 
			@RequestParam(value="linhasPorPagina", defaultValue = "10") Integer linhasPorPagina, 
			@RequestParam(value="ordenacao", defaultValue = "nome") String ordenacao, 
			@RequestParam(value="direcao", defaultValue = "ASC") String direcao) {
		Page<Categoria> lista = categoriaService.listarPaginado(pagina,linhasPorPagina,ordenacao, direcao);
		//converte a categoria em DTO para omitir a lista de produto da categori.
		//isso eh feito criando um construtor no DTO que nao possui a lista de produto
		//esse comando abaixo varrea a lista de categorias adicionando na lista de categoriaDTO:
		Page<CategoriaDTO> listaDTO = lista.map(obj -> new CategoriaDTO(obj)); 
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@Valid @RequestBody CategoriaDTO catDTO) {
		Categoria cat = categoriaService.convertDTO(catDTO);
		cat = categoriaService.salvar(cat);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cat.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value = "/{id}",method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@Valid @PathVariable Integer id, @Valid @RequestBody CategoriaDTO catDTO) {
		Categoria cat = categoriaService.convertDTO(catDTO);
		cat.setId(id);
		cat = categoriaService.atualizar(cat);
		
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<Void> remover(@PathVariable Integer id) {
		categoriaService.remover(id);
		
		return ResponseEntity.noContent().build();
	}

}
