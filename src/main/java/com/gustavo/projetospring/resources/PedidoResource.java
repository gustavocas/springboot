package com.gustavo.projetospring.resources;

import java.net.URI;

import javax.security.auth.login.LoginException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gustavo.projetospring.domain.Categoria;
import com.gustavo.projetospring.domain.Pedido;
import com.gustavo.projetospring.dto.CategoriaDTO;
import com.gustavo.projetospring.services.PedidoService;

@RestController
@RequestMapping(value="/pedidos")
public class PedidoResource {
	
	@Autowired
	private PedidoService pedidoService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {

			Pedido obj = pedidoService.buscar(id);			
			return ResponseEntity.ok().body(obj);
		
		
	}
	
	@RequestMapping(value = "/{nome}", method = RequestMethod.POST)
	public ResponseEntity<?> salvar(@PathVariable String nome) throws LoginException {
		
		Pedido ped = new Pedido();
		pedidoService.salvar(ped);
		
		return ResponseEntity.ok().body("Salvo com sucesso: " + ped.getId());
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> salvar(@Valid @RequestBody Pedido pedido) throws LoginException {
		
		pedido = pedidoService.salvar(pedido);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pedido.getId()).toUri();

		ResponseEntity res = ResponseEntity.created(uri).build();
		res.ok().body(pedido.getId());
		return res;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<Pedido>> listarPaginado(
			@RequestParam(value="pagina", defaultValue = "0") Integer pagina, 
			@RequestParam(value="linhasPorPagina", defaultValue = "10") Integer linhasPorPagina, 
			@RequestParam(value="ordenacao", defaultValue = "id") String ordenacao, 
			@RequestParam(value="direcao", defaultValue = "ASC") String direcao) throws LoginException {
		Page<Pedido> lista = pedidoService.listarPaginado(pagina,linhasPorPagina,ordenacao, direcao);
		return ResponseEntity.ok().body(lista);
	}

}
