package com.gustavo.projetospring.resources.exceptions;

import java.io.Serializable;

public class CampoValor implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String campo;
	private String valor;
	
	public CampoValor() {
		
	}
	
	public CampoValor(String campo, String valor) {
		super();
		this.campo = campo;
		this.valor = valor;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}

}
