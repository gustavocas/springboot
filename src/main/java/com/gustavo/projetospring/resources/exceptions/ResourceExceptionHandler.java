package com.gustavo.projetospring.resources.exceptions;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.gustavo.projetospring.services.exceptions.ChaveVioladaException;
import com.gustavo.projetospring.services.exceptions.ObjectNotFoundException;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<StandartError> objectNotFoundException(ObjectNotFoundException e, HttpServletRequest request){
		
		StandartError err = new StandartError(HttpStatus.NOT_FOUND.value(), "Nao encontrado.", System.currentTimeMillis(), e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
		
	}
	
	@ExceptionHandler(ChaveVioladaException.class)
	public ResponseEntity<StandartError> chaveViolada(ChaveVioladaException e, HttpServletRequest request){
		
		StandartError err = new StandartError(HttpStatus.BAD_REQUEST.value(), "Dados invalidos.", System.currentTimeMillis(), e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
		
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<StandartError> validacao(MethodArgumentNotValidException e, HttpServletRequest request){
		
		ErroValidacao err = new ErroValidacao(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Erro de validacao", System.currentTimeMillis(), e.getMessage(), request.getRequestURI());
		for (FieldError erro : e.getBindingResult().getFieldErrors()) {
			err.addicionarErro(erro.getField(), erro.getDefaultMessage());
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
		
	}
	
	@ExceptionHandler(LoginException.class)
	public ResponseEntity<StandartError> loginException(LoginException e, HttpServletRequest request){
		
		StandartError err = new StandartError(HttpStatus.FORBIDDEN.value(), "Acesso negado", System.currentTimeMillis(), e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(err);
		
	}
	
}
