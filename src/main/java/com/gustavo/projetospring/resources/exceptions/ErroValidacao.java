package com.gustavo.projetospring.resources.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ErroValidacao extends StandartError {

	private static final long serialVersionUID = 1L;
	
	private List<CampoValor> erros = new ArrayList<>();
	
	public ErroValidacao(Integer status, String mensagem, Long timeStamp, String error, String path) {
		super(status, mensagem, timeStamp, error, path);
	}

	public List<CampoValor> getErros() {
		return erros;
	}

	public void setErros(List<CampoValor> lista) {
		this.erros = lista;
	}
	
	public void addicionarErro(String campo, String valor) {
		this.erros.add(new CampoValor(campo, valor));
	}

}
