package com.gustavo.projetospring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gustavo.projetospring.domain.Pagamento;

@Repository
public interface PagametoRepository extends JpaRepository<Pagamento, Integer>{

}
