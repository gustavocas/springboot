package com.gustavo.projetospring.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gustavo.projetospring.domain.Categoria;
import com.gustavo.projetospring.domain.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer>{

	@Transactional(readOnly = true)
	@Query("SELECT distinct obj "
			+ " FROM Produto obj"
			+ " JOIN obj.categorias cat"
			+ " WHERE obj.nome LIKE %:nome% "
			+ " AND cat IN :categorias ")
	Page<Produto> buscar(@Param("nome") String nome, @Param("categorias") List<Categoria> categorias, Pageable pageReq);
	
	
	//esse metodo faz o mesmo que o de cima, usando os metodos do framework para pesquisa
	@Transactional(readOnly = true)
	Page<Produto> findDistinctByNomeContainsAndCategoriasIn(String nome, List<Categoria> categorias, Pageable pageReq);

}
