package com.gustavo.projetospring.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.gustavo.projetospring.services.DBService;
import com.gustavo.projetospring.services.EmailService;
import com.gustavo.projetospring.services.MockMailService;
import com.gustavo.projetospring.services.SmtpEmailService;

@Configuration
@Profile("dev")
public class DevConfig {
	
	@Autowired
	DBService dbService;
	
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String estrategiaCriacaoBanco; 

	@Bean
	public boolean instantiateDatabase() throws ParseException {
		
		if ("create".equals(estrategiaCriacaoBanco)) {
			dbService.instantiateTestDatabase();
		}
		return true;	
	}
	
	@Bean
	public EmailService emailService() {
//		return new SmtpEmailService(); //para enviar email de vdd tem q descomentar essa linha
		return new MockMailService();
	}
}
