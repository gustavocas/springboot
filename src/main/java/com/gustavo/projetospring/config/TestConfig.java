package com.gustavo.projetospring.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.gustavo.projetospring.services.DBService;
import com.gustavo.projetospring.services.EmailService;
import com.gustavo.projetospring.services.MockMailService;

@Configuration
@Profile("test")
public class TestConfig {
	
	@Autowired
	DBService dbService;

	@Bean
	public boolean instantiateDatabase() throws ParseException {
		dbService.instantiateTestDatabase();
		return true;	
	}
	
	@Bean
	public EmailService emailService() {
		return new MockMailService();
	}
}
