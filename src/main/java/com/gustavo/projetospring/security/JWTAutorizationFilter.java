package com.gustavo.projetospring.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAutorizationFilter extends BasicAuthenticationFilter {

	private JWTUtil jwtUtil;
	private UserDetailsService userDetalail;
	
	public JWTAutorizationFilter(AuthenticationManager authenticationManager, JWTUtil jwtUtil, UserDetailsService userDetalail) {
		super(authenticationManager);
		this.jwtUtil = jwtUtil;
		this.userDetalail = userDetalail;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException{
		String toekn = request.getHeader("Authorization");
		
		if(toekn != null && toekn.startsWith("Bearer")) {
			UsernamePasswordAuthenticationToken authenticationToken = getAutentication(request, toekn.substring(7));
			if (authenticationToken!=null) {
				SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			}
		}
		chain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAutentication(HttpServletRequest request, String token) {
		if(jwtUtil.tokenValido(token)) {
			String username = jwtUtil.getuserName(token);
			UserDetails details = userDetalail.loadUserByUsername(username);
			return new UsernamePasswordAuthenticationToken(username, null, details.getAuthorities());
		}
		return null;
	}

}
