package com.gustavo.projetospring.security;

import java.util.Date;

import org.hibernate.internal.ExceptionConverterImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtil {
	
	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.expiration}")
	private Long expiration;
	
	public String generateToken(String username) {
		return Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + expiration))
				.signWith(SignatureAlgorithm.HS512, secret.getBytes())
				.compact();
	}

	public boolean tokenValido(String token) {
		try{
			Claims claims = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
		
			if(claims!=null) {
				String username= claims.getSubject();
				Date dataExpiracao = claims.getExpiration();
				if(username!=null && dataExpiracao!=null&& dataExpiracao.getTime()>System.currentTimeMillis()) {
					return true;
				}
			}
		}catch(Exception e) {
			return false;
		}
		return false;
	}

	public String getuserName(String token) {
		Claims claims = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
		if(claims!=null) {
			String username= claims.getSubject();
			return username;
		}
		return null;
	}
}