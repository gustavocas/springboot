package com.gustavo.projetospring.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.gustavo.projetospring.domain.Cliente;
import com.gustavo.projetospring.services.validator.ClienteAtualizacao;

@ClienteAtualizacao
public class ClienteDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	@NotEmpty(message = "O nome nao pode ser vazio")
	@Size(min = 5, max = 30, message = "O nome deve ter entre 5 e 30 caracteres")
	private String nome;
	@NotEmpty(message = "O email nao pode ser vazio")
	@Email(message = "Informe um email valido")
	private String email;
	private String senha;
	private String foto;
	
	public ClienteDTO() {
	
	}
	
	public ClienteDTO(Cliente obj) {
		id=obj.getId();
		nome=obj.getNome();
		email=obj.getEmail();
		foto=obj.getFoto();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
}
