package com.gustavo.projetospring.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.gustavo.projetospring.services.validator.ClienteNovo;

@ClienteNovo
public class ClienteNovoDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "O nome nao pode ser vazio")
	@Size(min = 5, max = 30, message = "O nome deve ter entre 5 e 30 caracteres")
	private String nome;
	
	@NotEmpty(message = "O email nao pode ser vazio")
	@Email(message = "Informe um email valido")
	private String email;
	
	@NotEmpty(message = "O nome nao pode ser vazio")
	private String documento;
	private Integer tipo;
	
	//endereco
	@NotEmpty(message = "O nome nao pode ser vazio")
	private String logradouro;
	@NotEmpty(message = "O nome nao pode ser vazio")
	private String numero;
	private String complemento;
	private String bairro;
	@NotEmpty(message = "O nome nao pode ser vazio")
	private String cep;
	
	private Integer idCidade;
	
	@NotEmpty(message = "O nome nao pode ser vazio")
	private String telefone1;
	private String telefone2;
	private String telefone3;
	
	@NotEmpty(message = "Senha nao pode ser vazia")
	private String senha;
	private String foto;
	
	public ClienteNovoDTO() {
	
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Integer getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getTelefone3() {
		return telefone3;
	}

	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	
	
}
