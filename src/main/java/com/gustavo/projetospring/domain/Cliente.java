package com.gustavo.projetospring.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gustavo.projetospring.domain.enums.PerfilCliente;
import com.gustavo.projetospring.domain.enums.TipoCliente;

@Entity
public class Cliente implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nome;
	private String email;
	private String documento;
	//private Integer tipo;
	@JsonIgnore //para nao mostrar a senha no json
	private String senha;
	@Column(name="DESC", nullable=true, length=15000000)
	private String foto;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL) //cascade = CascadeType.ALL faz remover os registro associados
	private List<Endereco> enderecos = new ArrayList<>();
	
	@ElementCollection
	@CollectionTable(name = "clientetelefone")
	private Set<String> telefones = new HashSet<>();
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "perfilcliente")
	private Set<Integer> perfilCliente = new HashSet<>();
	
	@JsonIgnore //nao faz a busca automatica evitando referencia ciclica
	@OneToMany(mappedBy = "cliente")
	private List<Pedido> pedidos = new ArrayList<>();
	
	public Cliente(Integer id, String nome, String email, String documento, TipoCliente tipo, String senha, String foto) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.documento = documento;
//		this.tipo = tipo.getCodigo();
		this.senha = senha;
		this.foto=foto;
		addPerfilCliente(PerfilCliente.CLIENTE); //todo cliente eh iniciado com perfil cliente
	}

	public Cliente() {
		addPerfilCliente(PerfilCliente.CLIENTE); //todo cliente eh iniciado com perfil cliente
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

//	public TipoCliente getTipo() {
//		return TipoCliente.toEnum(tipo);
//	}

//	public void setTipo(TipoCliente tipo) {
//		this.tipo = tipo.getCodigo();
//	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Set<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(Set<String> telefones) {
		this.telefones = telefones;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

//	public void setTipo(Integer tipo) {
//		this.tipo = tipo;
//	}

	public Set<PerfilCliente> getPerfilCliente() {
		return perfilCliente.stream().map(x -> PerfilCliente.toEnum(x)).collect(Collectors.toSet());
	}
	
	public void addPerfilCliente(PerfilCliente perfilCliente) {
		this.perfilCliente.add(perfilCliente.getCodigo());
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
}
