package com.gustavo.projetospring.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.gustavo.projetospring.domain.enums.EstadoPagamento;

@Entity
@JsonTypeName("PagamentoCartao") //mapeado pelo @JsonTypeInfo na classe pagamento
public class PagamentoCartao extends Pagamento{
	private static final long serialVersionUID = 1L;
	
	private Integer numeroParcelas;
	
	public PagamentoCartao(Integer id, EstadoPagamento estado, Pedido pedido, Integer numeroParcelas) {
		super(id, estado, pedido);
		this.setNumeroParcelas(numeroParcelas);
	}
	
	public PagamentoCartao() {
		
	}

	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
	
	
}
