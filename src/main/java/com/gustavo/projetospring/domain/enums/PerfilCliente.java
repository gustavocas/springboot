package com.gustavo.projetospring.domain.enums;

public enum PerfilCliente {
	ADMIN(1,"ROLE_ADMIN"), //o "ROLE_" eh exigencia do framework??
	CLIENTE(2, "ROLE_CLIENTE");
	
	private int codigo;
	private String descricao;

	private PerfilCliente(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public String getDescricao() {
		return descricao;
	}

	public static PerfilCliente toEnum(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		
		for (PerfilCliente tipo : PerfilCliente.values()) {
			if (tipo.codigo == codigo) {
				return tipo;
			}
		}
		
		throw new IllegalArgumentException("Codigo invalido: " + codigo);
	}
	
	
}
