gera o jar do projeto:
mvn install -DskipTests

Executar no terminal:
java -Dserver.port=$PORT -Dspring.profiles.active=dev $JAVA_OPTS -jar projetoSpring-0.0.1-SNAPSHOT.jar

onde:
-Dspring.profiles.active=dev
pode ser "dev" para perfil de desenvolvimento com banco mysqlou "test" para perfil teste com banco h2

POST pedidos:
http://localhost:8080/pedidos/
	{
	"cliente" :     {"id" : 1},
	"enderecoEntrega" : {"id" : 1},
	"pagamento" : {
	    "numeroDeParcelas" : 10,
	    "@type": "PagamentoCartao"
	},
	"itens" : [
	{
	"quantidade" : 2,
	"produto" : {"id" : 3}
	},
	{
	"quantidade" : 1,
	"produto" : {"id" : 1}
	}
	]
	}
	
POST clientes:
http://localhost:8080/clientes/
{
    
    "nome": "regina",
    "email": "reg@gmail",
    "documento": "444000",
    "tipo": "1",
    "logradouro": "rua paschoalito",
	"numero": "405",
	"complemento" : "nao tem" ,
	"bairro":  "nova deli",
	"cep": "12349449",
	"idCidade":"1",
    "senha":"1",
    "documento":"33333333333",
	"telefone1": "34418090"
}

POST usuario (se usuario nao contiver gustavo da erro)
{
    "email": "gustavo@gmail",
    "senha":"1"
}